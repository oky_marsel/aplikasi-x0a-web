<?php 
	include("show_data.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Web Page Aplikasi x0a</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php">Aplikasi X0a Web</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Menu
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <a class="dropdown-item" type="button" href="mahasiswa.php">Tabel Mahasiswa</a>
                <a class="dropdown-item" type="button" href="prodi.php">Tabel Prodi</a>
            </div>
        </div>
    </nav>
    <div class="container">
        <header>
            <h1><center>Daftar Mahasiswa<center></h1>
        </header>
        <table class="table table-bordered table-hover">
		    <thead>
			    <tr>
				    <th>No</th>
				    <th>NIM</th>
				    <th>Nama</th>
				    <th>Prodi</th>
				    <th>Foto</th>
				    <th>Alamat</th>
			    </tr>
		    </thead>
            <tbody>
			    <?php 
				    $sql = "SELECT m.nim, m.nama, p.nama_prodi,m.poto, m.alamat
                    FROM mahasiswa m, prodi p
                    WHERE m.id_prodi=p.id_prodi
                    ORDER BY p.id_prodi";
                    $query = mysqli_query($db,$sql);
                    $no=1;
                    while ($mhs = mysqli_fetch_array($query)) {
                        echo "<tr>";
                        echo "<td>".$no;$no++;"</td>";
                        echo "<td>".$mhs['nim']."</td>";
                        echo "<td>".$mhs['nama']."</td>";
                        echo "<td>".$mhs['nama_prodi']."</td>";
                        echo "<td>"."<img src='img/$mhs[poto]' width='50' height='50' />"."</td>";
                        echo "<td>".$mhs['alamat']."</td>";
                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>