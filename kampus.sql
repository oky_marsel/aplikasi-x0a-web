/*
 Navicat Premium Data Transfer

 Source Server         : okymarsel
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : kampus

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 30/04/2020 11:23:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa`  (
  `nim` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nama` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id_prodi` int(11) NOT NULL,
  `poto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`nim`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mahasiswa
-- ----------------------------
INSERT INTO `mahasiswa` VALUES ('1931733067', 'Bagas Dwi Santoso', 2, 'bagas.jpg', 'Ngadiluwih - Kediri');
INSERT INTO `mahasiswa` VALUES ('1931733068', 'Rizka Dewi M Y', 3, 'rizka.jpg', 'Ngadisimo - Kediri');
INSERT INTO `mahasiswa` VALUES ('1931733083', 'Moch Adnan Rizaldi', 1, 'blu.jpg', 'Rejoso - Nganjuk');
INSERT INTO `mahasiswa` VALUES ('1931733084', 'Lely Friscilla', 3, 'lely.jpg', 'Plosoklaten - Kediri');
INSERT INTO `mahasiswa` VALUES ('1931733090', 'Eriawan Santoso', 1, 'eri.jpg', 'Gampeng - Kediri');

-- ----------------------------
-- Table structure for prodi
-- ----------------------------
DROP TABLE IF EXISTS `prodi`;
CREATE TABLE `prodi`  (
  `id_prodi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_prodi` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id_prodi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of prodi
-- ----------------------------
INSERT INTO `prodi` VALUES (1, 'Manajemen Informatika');
INSERT INTO `prodi` VALUES (2, 'Teknik Mesin');
INSERT INTO `prodi` VALUES (3, 'Akuntansi');

SET FOREIGN_KEY_CHECKS = 1;
